<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<!-- <link href="css/style.css" rel="stylesheet"> -->
<title>Insert title here</title>
</head>
<body>
    <h1>Bienvenue sur ma page d'accueil !!!</h1>
    <c:out value="Hello world!" ></c:out>
    <h3>${nomCoteJSP}</h3>
    <a href="cgv?page=maDonneeEnvoye">Gonditions Generales de Ventes</a>
    
     
    <div>${resultat}</div>
    
    <table border="1">
        <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Code Postal</th>
            <th>Nombre touristes</th>
            <th>Département</th>
        </tr>
        <c:forEach items="${resultat}" var="ville">
            <tr>
                <td>${ville.idVille}</td>
                <td>${ville.nom}</td>
                <td>${ville.codePostal}</td>
                <td>${ville.nbTourristesAnnuel}</td>
                <td>${ville.idDepartement}</td>
            </tr>
        </c:forEach>
    </table>
    
</body>
</html>