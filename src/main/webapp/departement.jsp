<%@ page language="java" contentType="text/html; charset=UTF-8"                  
    pageEncoding="UTF-8"%>                                                       
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>                  
<!DOCTYPE html>                                                                  
<html>                                                                           
<head>                                                                           
<meta charset="UTF-8">                                                           
<title>Départements</title>                                                            
</head>                                                                          
<body>                                                                           
<h1>Liste des départements</h1>                                                        
                                                                                 
<%-- <div>${resultat}</div> --%>                                                 
 <table border="1">                                                              
        <tr>                                                                     
            <th>ID</th>                                                          
            <th>Numéro</th>                                                         
            <th>Nom</th>                                                 
            <th>Résponsable départemental : Nom</th>                                            
            <th>Résponsable départemental : Préom</th>                                                 
        </tr>                                                                    
        <c:forEach items="${resultat}" var="departement">                              
            <tr>                                                                 
                <td>${departement.id_departement}</td>                                  
                <td>${departement.getNumero_departement()}</td>                                       
                <td>${departement.getNom_departement()}</td>                                
                <td>${departement.getAgent_dep_nom()}</td>                         
                <td>${departement.getAgent_dep_prenom()}</td>                            
            </tr>                                                                
        </c:forEach>                                                             
    </table>                                                                     
</body>                                                                          
</html>                                                                          