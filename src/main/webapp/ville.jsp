<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Villes</title>
</head>
<body>
<h1>Liste des villes</h1>

<%-- <div>${resultat}</div> --%>
 <table border="1">
        <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Code Postal</th>
            <th>Nombre touristes</th>
            <th>Département</th>
        </tr>
        <c:forEach items="${resultat}" var="ville">
            <tr>
                <td>${ville.getId_ville()}</td>
                <td>${ville.getNom()}</td>
                <td>${ville.getCodePostal()}</td>
                <td>${ville.getNbTouristesAnnuel()}</td>
                <td>${ville.getId_departement()}</td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>