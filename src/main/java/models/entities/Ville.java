package models.entities;

public class Ville {
	private int id_ville;
	private int id_departement;
	private String codePostal;
	private String nom;
	private int NbTouristesAnnuel;
	
	// Getters and Setters
	public int getId_departement() {
		return id_departement;
	}
	public void setId_departement(int id_departement) {
		this.id_departement = id_departement;
	}	
	public int getId_ville() {
		return id_ville;
	}
	public void setId_ville(int id_ville) {
		this.id_ville = id_ville;
	}	
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getNbTouristesAnnuel() {
		return NbTouristesAnnuel;
	}
	public void setNbTouristesAnnuel(int nbTouristesAnnuel) {
		NbTouristesAnnuel = nbTouristesAnnuel;
	}
	
	// Constructeurs
	public Ville(int id_ville, int id_departement, String codePostal, String nom, int nbTouristesAnnuel) {
		this.id_ville = id_ville;
		this.id_departement = id_departement;
		this.codePostal = codePostal;
		this.nom = nom;
		this.NbTouristesAnnuel = nbTouristesAnnuel;
	}
	public Ville(String codePostal, String nom, int nbTouristesAnnuel) {
		this.codePostal = codePostal;
		this.nom = nom;
		this.NbTouristesAnnuel = nbTouristesAnnuel;
	}
	public Ville(int id_ville,  int nbTouristesAnnuel) {
		this.id_ville = id_ville;
		this.NbTouristesAnnuel = nbTouristesAnnuel;
	}
	public Ville() {};
	
	// ToString
	@Override
	public String toString() {
		return "Ville [CP :" + codePostal + ", nom =" + nom + ", NbTouristesAnnuel=" + NbTouristesAnnuel + "]";
	}


}
