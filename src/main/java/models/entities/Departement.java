package models.entities;

public class Departement {
	private int id_departement;
	private String numero_departement;
	private String nom_departement;
	private String agent_dep_nom;
	private String agent_dep_prenom;
	
	
	// Getters and Setters
	public int getId_departement() {
		return id_departement;
	}
	public void setId_departement(int id_departement) {
		this.id_departement = id_departement;
	}
	public String getNumero_departement() {
		return numero_departement;
	}
	public void setNumero_departement(String numero_departement) {
		this.numero_departement = numero_departement;
	}
	public String getNom_departement() {
		return nom_departement;
	}
	public void setNom_departement(String nom_departement) {
		this.nom_departement = nom_departement;
	}
	public String getAgent_dep_nom() {
		return agent_dep_nom;
	}
	public void setAgent_dep_nom(String agent_dep_nom) {
		this.agent_dep_nom = agent_dep_nom;
	}
	public String getAgent_dep_prenom() {
		return agent_dep_prenom;
	}
	public void setAgent_dep_prenom(String agent_dep_prenom) {
		this.agent_dep_prenom = agent_dep_prenom;
	}
	
	// Constructeurs
	public Departement(int id_departement, String numero_departement, String nom_departement, String agent_dep_nom,
			String agent_dep_prenom) {
		this.id_departement = id_departement;
		this.numero_departement = numero_departement;
		this.nom_departement = nom_departement;
		this.agent_dep_nom = agent_dep_nom;
		this.agent_dep_prenom = agent_dep_prenom;
	}	
	public Departement(String numero_departement, String nom_departement) {
		super();
		this.numero_departement = numero_departement;
		this.nom_departement = nom_departement;
	}	
	public Departement(int id_departement, String agent_dep_nom, String agent_dep_prenom) {
		super();
		this.id_departement = id_departement;
		this.agent_dep_nom = agent_dep_nom;
		this.agent_dep_prenom = agent_dep_prenom;
	}
	public Departement() {};
	
	// ToString
	@Override
	public String toString() {
		return "Departement [N°:" + numero_departement 
				+ ", Nom departement" + nom_departement
				+ ", Nom agent departement=" + agent_dep_nom 
				+ ", Prenom agent departement=" + agent_dep_prenom + "]";
	}	

}
