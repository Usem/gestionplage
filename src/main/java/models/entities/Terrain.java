package models.entities;

public class Terrain {
	private int id_terrain;
	private String natureGeo;

	
	// Getters and Setters
	
	public int getId_terrain() {
		return id_terrain;
	}
	public void setId_terrain(int id_terrain) {
		this.id_terrain = id_terrain;
	}	
	public String getNatureGeo() {
		return natureGeo;
	}
	public void setNatureGeo(String natureGeo) {
		this.natureGeo = natureGeo;
	}
	
	// Constructeurs
	public Terrain(int id_terrain, String natureGeo) {
		super();
		this.id_terrain = id_terrain;
		this.natureGeo = natureGeo;
	}
	public Terrain() {};
    
	// ToString
	@Override
	public String toString() {
		return "Terrain [nature =" + natureGeo + "]";
	}

	
	

}
