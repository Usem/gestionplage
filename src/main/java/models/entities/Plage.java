package models.entities;

public class Plage {
	private int id_plage;
	private int id_ville;
	private double longueurKM;

	// Getters and Setters
	public int getId_plage() {
		return id_plage;
	}

	public void setId_plage(int id_plage) {
		this.id_plage = id_plage;
	}

	public int getId_ville() {
		return id_ville;
	}

	public void setId_ville(int id_ville) {
		this.id_ville = id_ville;
	}

	public double getLongueurKM() {
		return longueurKM;
	}

	public void setLongueurKM(double longueurKM) {
		this.longueurKM = longueurKM;
	}	
	
    // Constructeurs
	public Plage(int id_plage, int id_ville, double longueurKM) {
		super();
		this.id_plage = id_plage;
		this.id_ville = id_ville;
		this.longueurKM = longueurKM;
	}
	public Plage(int id_plage,double longueurKM) {
		this.id_plage = id_plage;
		this.longueurKM = longueurKM;
	}	
	public Plage() {};
	
    // ToString
	@Override
	public String toString() {
		return "Plage [Longueur (Km) =" + longueurKM + "]";
	}

}
