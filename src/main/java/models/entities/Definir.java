package models.entities;

public class Definir {
	private int id_plage;
	private int id_terrain;
	private int pourcentage;
	
	public int getId_plage() {
		return id_plage;
	}
	public void setId_plage(int id_plage) {
		this.id_plage = id_plage;
	}
	public int getId_terrain() {
		return id_terrain;
	}
	public void setId_terrain(int id_terrain) {
		this.id_terrain = id_terrain;
	}
	public int getPourcentage() {
		return pourcentage;
	}
	public void setPourcentage(int pourcentage) {
		this.pourcentage = pourcentage;
	}
	
	//Constructeurs
	public Definir(int id_plage, int id_terrain, int pourcentage) {
		this.id_plage = id_plage;
		this.id_terrain = id_terrain;
		this.pourcentage = pourcentage;
	}
	public Definir() {};

	//ToString
	@Override
	public String toString() {
		return "Definir [pourcentage =" + pourcentage + " % ]";
	}


	

}
