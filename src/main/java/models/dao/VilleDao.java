package models.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import models.SingletonConnection;
import models.entities.Ville;

public class VilleDao {
	
	public List<Ville> getVilles(){
		Connection conn = SingletonConnection.getConnection();
		
		List<Ville> villes = new ArrayList<Ville>();
		try {
			// Préparation de la requête
	        String requete = "SELECT * FROM ville";
	        PreparedStatement ps = conn.prepareStatement(requete);
	          
	        // On execute la requête et récupère les resultats
	        ResultSet rs = ps.executeQuery();
	          
	        System.out.println("Connected to BDD : table:ville");
	        System.out.println(rs);

	        while (rs.next()) {
	        	Ville v = new Ville(rs.getInt("id_ville"), rs.getInt("FK_Departement"), rs.getString("NomVille"), rs.getString("CodePostal"), rs.getInt("NBTouristes"));
	            villes.add(v);
	        }	                  
	        ps.close();	          
	    }
	    catch (Exception e) {
	        e.printStackTrace();
	    }
		
		return villes;
		
	}

}
