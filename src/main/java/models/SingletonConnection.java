package models;

import java.sql.Connection;
import java.sql.DriverManager;

public class SingletonConnection {
	private static Connection connection; 
	
	public static Connection getConnection() {
		return connection;
	}
	
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/gestionPlages?characterEncoding=utf8","larabih","LH2107TF2208");
		}
		catch (Exception e) {
			e.printStackTrace();  
		}
	}

}
