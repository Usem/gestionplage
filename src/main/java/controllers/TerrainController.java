package controllers;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



@WebServlet(name="terrain",urlPatterns = {"/terrain"})
public class TerrainController extends HttpServlet {
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
	  
    RequestDispatcher dispatcher = request.getRequestDispatcher("terrain.jsp");
    dispatcher.forward(request, response);
  }
}