package controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.dao.VilleDao;
import models.entities.Ville;

@SuppressWarnings("serial")
@WebServlet(name="ville",urlPatterns = {"/ville"})
public class VilleController extends HttpServlet {
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
	  VilleDao villeDao = new VilleDao();
	  List<Ville> villes = villeDao.getVilles();
	  
	  request.setAttribute("resultat", villes);

	  request.getRequestDispatcher("ville.jsp").forward(request, response);

	  
//    RequestDispatcher dispatcher = request.getRequestDispatcher("ville.jsp");
//    dispatcher.forward(request, response);
  }
}