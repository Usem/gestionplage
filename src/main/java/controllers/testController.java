package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.SingletonConnection;
import models.entities.Ville;



@WebServlet(name="test",urlPatterns = {"/test"})
public class testController extends HttpServlet {
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
	  
	  Connection conn = SingletonConnection.getConnection();
	  System.out.println(conn);
	  
	  try {
          // Préparation de la requête
          String requete = "SELECT * FROM ville";
          PreparedStatement ps = conn.prepareStatement(requete);
          
          // On execute la requête et récupère les resultats
          ResultSet rs = ps.executeQuery();
          
          System.out.println("Connected to BDD :");
          System.out.println(rs);

          List<Ville> villes = new ArrayList<Ville>();
          while (rs.next()) {
              Ville v = new Ville(rs.getInt("id_ville"), rs.getInt("NBTouristes"), rs.getString("NomVille"), rs.getString("CodePostal"), rs.getInt("ID_Departement"));
              villes.add(v);
          }
                  
          request.setAttribute("resultat",villes);
          
      }
      catch (Exception e) {
          e.printStackTrace();
      }
	   
    RequestDispatcher dispatcher = request.getRequestDispatcher("test.jsp");
    dispatcher.forward(request, response);
  }
}
