package controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.SingletonConnection;
import models.entities.Departement;



@WebServlet(name="departement",urlPatterns = {"/departement"})
public class DepartementController extends HttpServlet {
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
	  
	  Connection conn = SingletonConnection.getConnection();

	  try {
          // Préparation de la requête
          String requete = "SELECT * FROM departement";
          PreparedStatement ps = conn.prepareStatement(requete);
          
          // On execute la requête et récupère les resultats
          ResultSet rs = ps.executeQuery();
          
          System.out.println("Connected to BDD : table:departement");
          System.out.println(rs);

          List<Departement> departements = new ArrayList<Departement>();
          while (rs.next()) {
        	  Departement d = new Departement (rs.getInt("ID_Departement"), 
        			  rs.getString("NumDepartement"), rs.getString("NomDepartement"),
        			  rs.getString("NomResponsable"), rs.getString("PrenomResponsable"));
        	  departements.add(d);
          }
                  
          request.setAttribute("resultat", departements);
          request.getRequestDispatcher("departement.jsp").forward(request, response);

          ps.close();
          
      }
      catch (Exception e) {
          e.printStackTrace();
      }
	  
//    RequestDispatcher dispatcher = request.getRequestDispatcher("departement.jsp");
//    dispatcher.forward(request, response);
  }
}
